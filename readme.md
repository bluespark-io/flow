# Flow

## Synopsis

Flow is a base class library for F# that wraps various .NET Framework functionality
so that it is more natural to consume from F#.

## Build Status

Branch | Build Status
-------|-------------
master | [![bluespark-io MyGet Build Status](https://www.myget.org/BuildSource/Badge/bluespark-io?identifier=61e6578c-0753-4a3f-9b9a-dbb63432be18)](https://www.myget.org/)
