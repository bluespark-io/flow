namespace Flow

module Seq =
  let chunks size (sequence : 'a seq) =
    seq {
      use enum = sequence.GetEnumerator ()

      let rec loop remaining acc =
        seq {
          match remaining with
          | 0 ->
            yield acc
            yield! loop size []
          | _ ->
            match enum.MoveNext () with
            | true ->
              yield! loop (remaining - 1) (enum.Current :: acc)
            | false ->
              match acc with
              | [] -> ()
              | _ -> yield acc
        }

      yield! loop size []
    }
