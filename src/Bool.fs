namespace Flow

module Bool =

  /// Represents a predicate function
  type 'a predicate =
    'a -> bool

  /// Constructs a predicate whose result is the logical-and
  /// of the results of two given predicates.
  let composeAnd (f: 'a predicate) (g: 'a predicate) : 'a predicate =
    fun x -> f x && g x

  /// The operator form of <see cref="composeAnd" />.
  let (<&&>) =
    composeAnd

  /// Constructs a predicate whose result is the logical-or
  /// of the results of two given predicates.
  let composeOr (f: 'a predicate) (g: 'a predicate) : 'a predicate =
    fun x -> f x || g x

  /// The operator form of <see cref="composeOr" />.
  let (<||>) =
    composeOr
