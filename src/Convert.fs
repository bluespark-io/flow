﻿namespace Flow

type Base64Encoding =
  | StandardEncoding
  | UrlEncoding

module Convert =
  let private charSetStandard = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="
  let private charSetUrl = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_="

  let private emit (charSet:string) (a:byte) (b:byte) (c:byte) (padCount:int) =
    let bits = (int a <<< 16) ||| (int b <<< 08) ||| (int c) 

    let outA = (bits &&& (63 <<< 18)) >>> 18
    let outB = (bits &&& (63 <<< 12)) >>> 12
    let outC = if padCount > 1 then 64 else (bits &&& (63 <<< 06)) >>> 06
    let outD = if padCount > 0 then 64 else (bits &&& (63 <<< 00)) >>> 00

    [charSet.[outA]; charSet.[outB]; charSet.[outC]; charSet.[outD]]

  let private base64OfChunk charSet arr =
    match arr with
      | [a; b; c] -> emit charSet a b c 0
      | [a; b] -> emit charSet a b 0uy 1
      | [a] -> emit charSet a 0uy 0uy 2
      | _ -> raise <| System.ArgumentException (sprintf "arr must have a length of 1, 2, or 3, its actual length is %i." (List.length arr))

  let toBase64 (encoding : Base64Encoding) (seq : byte seq) = 
    let charSet =
      match encoding with
        | StandardEncoding -> charSetStandard
        | UrlEncoding -> charSetUrl

    seq |> Seq.chunks 3 |> Seq.map (base64OfChunk charSet) |> Seq.concat
