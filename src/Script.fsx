﻿// Learn more about F# at http://fsharp.net. See the 'F# Tutorial' project
// for more guidance on F# programming.
#load "Seq.fs"
#load "Crypto.fs"
#load "Convert.fs"
#load "String.fs"

open Flow

printfn "Random: %A" (Crypto.strongRandom () |> Convert.toBase64 StandardEncoding |> Seq.take 28 |> String.fromChars)
