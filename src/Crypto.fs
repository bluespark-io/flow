﻿namespace Flow

module Crypto =

  open System.Security.Cryptography

  let strongRandom () =
    seq {
      use rng = new RNGCryptoServiceProvider ()
      let buffer = Array.zeroCreate 1
      while true do
          rng.GetBytes (buffer) 
          yield buffer.[0]
      }
