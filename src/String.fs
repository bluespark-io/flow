namespace Flow

type StringComparisonMode =
  | OrdinalIgnoreCase

module String =
  let (|Match|) comparisonMode left right =
    match comparisonMode with
      | OrdinalIgnoreCase -> System.StringComparer.OrdinalIgnoreCase.Equals(left, right)

  let fromChars (chars : char seq) : string =
    chars |> System.String.Concat
