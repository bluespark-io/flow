namespace Flow

module Interop =
  open System.Collections.Generic

  /// <summary>
  /// Constructs a <see cref="System.KeyValuePair<K, V>" />.
  /// </summary>
  let kv key value = KeyValuePair<_, _>(key, value)
