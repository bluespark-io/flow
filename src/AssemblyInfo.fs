﻿module Flow.AssemblyInfo

open System.Reflection
open System.Runtime.CompilerServices

[<assembly:AssemblyTitle("Flow")>]
[<assembly:AssemblyDescription("A BCL for F#")>]
[<assembly:AssemblyCompany("bluespark.io")>]
[<assembly:AssemblyProduct("Flow")>]
[<assembly:AssemblyCopyright("Philip Stears")>]
[<assembly:AssemblyVersion("1.0.0.0")>]
()

