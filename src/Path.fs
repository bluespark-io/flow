﻿namespace Flow

module Path =
  open System.IO

  let append pathToAppend basePath = Path.Combine(basePath, pathToAppend)

  let abs path = Path.GetFullPath path

  let withTrailingDirectorySeparator (path: string) =
    let isDirSep ch =
      ch = Path.DirectorySeparatorChar ||
      ch = Path.AltDirectorySeparatorChar

    match path.Length with
      | 0 -> string Path.DirectorySeparatorChar
      | n ->
        match isDirSep path.[n - 1] with
          | true -> path
          | false -> path + string Path.DirectorySeparatorChar
