namespace Flow

module Http = 
  let uriEncodeData value =
    System.Uri.EscapeDataString value

  let joinParams queryParams =
    queryParams |>
      Seq.collect (fun (key, value) -> ["&"; uriEncodeData key; "="; uriEncodeData value]) |>
      Seq.skip 1 |>
      String.concat "" 

  let buildUri baseUri queryParams =
    baseUri + "?" + (joinParams queryParams)
