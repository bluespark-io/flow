namespace Flow

module Configuration =

  /// Retrieves a setting from the appSettings section of
  /// the app domain's configuration.
  let setting (name : string) : string =
    System.Configuration.ConfigurationManager.AppSettings.[name]
