﻿namespace Flow

module IO =
  open System.IO

  let fileExists path = File.Exists path
