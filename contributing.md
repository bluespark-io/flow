# Contributing

TODO

# Philosophical Guidelines

1. Functions which mutate some entity should take the entity as the last
   parameter so that the entity can be piped in, e.g. `Path.append` takes the
   base path as the **final parameter**.

2. Lazily evaluated sequences are usually preferable over strict collection
   types because they compose naturally, for example `Seq.chunks` and `Crypto.strongRandom`.

# Coding Guidelines

1. Public functions should be annotated with types.
